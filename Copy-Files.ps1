﻿# This script is for copy files to vtank directory, replacing any conflicts
$VTankFolder="C:\Games\VirindiPlugins\VirindiTank\" # Should usually be C:\Games\VirindiPlugins\VirindiTank\

# Update Files
Get-Childitem -Path "$PSScriptRoot\*" -Exclude *.ps1,.gitignore,.gitattributes,*.md|
    Copy-Item -Destination $VTankFolder -Force -Verbose
