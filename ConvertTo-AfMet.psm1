# Powershell module for converting between .met and .af
# usage import module ConvertTo-AFMet.psm1
# ConvertTo-AF "MetaFilePath.met"
# ConvertTo-Met "Afpath.af"

# Configurable Params
$MetaFBinaryPath = "C:\Games\MetaAF\bin\metaf.exe"  #Path to extracted metaf.exe
$VTankDirectory = "C:\Games\VirindiPlugins\VirindiTank" # Default is C:\Games\VirindiPlugins\VirindiTank
$AutoCopyMet = $True #whether to automatically copy .met files to the vtank directory ($True|$False)

Function ConvertTo-Af {
    $InputFile = $args
    $InputFileProperties = get-ItemProperty -path "$InputFile"
    if ($OutputFile){Clear-Variable -Name OutputFile} #Clear the variable incase running the script in a console window.
    if (($InputFileProperties.FullName).EndsWith(".met")){
        $OutputFile = ($InputFileProperties.FullName -replace ".met$")+(".af")  
        Write-Output "Input File $InputFileProperties"|Out-Host
        &$MetaFBinaryPath "$InputFile" "$OutputFile" |Out-Host
    } ELSE {
        Write-Output  "File extension does not appear to end with .met??? Cannot continue"|Out-Host
    }
}
Function ConvertTo-Met {
    $InputFile = $args
    $InputFileProperties = get-ItemProperty -path "$InputFile"
    if ($OutputFile){Clear-Variable -Name OutputFile} #Clear the variable incase running the script in a console window.
    if (($InputFileProperties.FullName).EndsWith(".af")){
        $OutputFile = ($InputFileProperties.FullName -replace ".af$")+(".met")
        Write-Output "Input File $InputFileProperties"|Out-Host
        &$MetaFBinaryPath "$InputFile" "$OutputFile" |Out-Host
        if ($AutoCopyMet -eq $True){
            Copy-Item -Path $OutputFile -Destination "$VTankDirectory\" -Verbose
        }
    } ELSE {
        Write-Output  "File extension does not appear to end with .af??? Cannot continue"|Out-Host
    }
}