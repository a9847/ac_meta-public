# List of other useful metas

Here is a list of other useful metas that I sporadically use from the following page,
http://www.virindi.net/wiki/index.php/Public_Meta_Repository

- IBControl v1.1.2 - By Immortalbob
    - Incredibly useful for controlling multiple characters simultaneously via a leader character.
- Aun Ralirea - By Dmdtt
- Rare Exchanger - By Immortalbob
- Advanced Colo - By Dmdtt
- Caul Rambler - By Immortalbob
    - Good for some caul based LBP requests



Other useful Metas
- [PyrealBarMaker](http://www.immortalbob.com/phpBB3/viewtopic.php?t=86)
- [AutoCrafter](https://github.com/immortalbob/AutoCrafter)